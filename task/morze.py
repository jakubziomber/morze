def code_morze(value):
    morse_code_dict = {
        'A': '.-', 'B': '-...', 'C': '-.-.', 'D': '-..', 'E': '.',
        'F': '..-.', 'G': '--.', 'H': '....', 'I': '..', 'J': '.---',
        'K': '-.-', 'L': '.-..', 'M': '--', 'N': '-.', 'O': '---',
        'P': '.--.', 'Q': '--.-', 'R': '.-.', 'S': '...', 'T': '-',
        'U': '..-', 'V': '...-', 'W': '.--', 'X': '-..-', 'Y': '-.--',
        'Z': '--..', '1': '.----', '2': '..---', '3': '...--', '4': '....-',
        '5': '.....', '6': '-....', '7': '--...', '8': '---..', '9': '----.',
        '0': '-----', ',': '--..--', '.': '.-.-.-', '?': '..--..', '/': '-..-.',
        '-': '-....-', '(': '-.--.', ')': '-.--.-'
    }

    value = value.upper()  # Convert input value to uppercase
    morse_code_list = []

    for char in value:
        if char == ' ':
            morse_code_list.append(' ')
        elif char in morse_code_dict:
            morse_code_list.append(morse_code_dict[char])
    
    # Join the Morse code characters without spaces and then add spaces between words
    morse_code_message = ' '.join(morse_code_list).replace('  ', ' ')
    
    return morse_code_message

# Example usage
input_value = "Data Science-2022"
morse_code = code_morze(input_value)
print(morse_code)